import Constants from '../commons/constants.js';
import Utils from '../commons/utils.js';
import User from './user.js';

class UserController {

  constructor() {

    this.$userList = document.querySelector('.user-list');
    this.currentPage = -1;
  }

  /**
  * getByUserName()
  * call gitlab user rest service
  * @param username: search user for the username
  * @param callback: function to be called after service returns
  */
  getByUserName(username = '', callback) {

    Utils.load(`${Constants.REST_URL}/users?page=${++this.currentPage}&per_page=${Constants.PAGE_SIZE}&search=${username}`, result => {
      callback(result.map(c => new User(c)));
      //scroll to top in case it is a new search (not a new page)
      !!!this.currentPage && window.scrollTo(0, Math.min(90, window.scrollY));
    });
  }

  /**
  * clear()
  * emptys the users list DOM element and reset the current page
  */
  clear() {

    this.$userList.innerHTML = '';
    this.currentPage = -1;
  }

  /**
  * renderAll()
  * append list of users to the DOM element
  * @param users: list of users
  * @see user.js
  */
  renderAll(users) {

    //console.log(users);
    users.forEach(user => this.$userList.appendChild(user.render()));
  }
}

export default new UserController();


