/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/commons/constants.js":
/*!**********************************!*\
  !*** ./src/commons/constants.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({

  //gitlab rest service token
  TOKEN: 'dGqiMdPF3QAKsPcpayN1',

  //gitlab rest url
  REST_URL: 'https://gitlab.com/api/v4',

  //users per page
  PAGE_SIZE: 20,

  //minimum delay beetwen service calls
  SEARCH_DELAY: 500

});

/***/ }),

/***/ "./src/commons/utils.js":
/*!******************************!*\
  !*** ./src/commons/utils.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _commons_constants_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../commons/constants.js */ "./src/commons/constants.js");


class Utils {

  constructor() {

    this.lastCallTime = 0;
  }

  /**
  * replaceTemplateProperties()
  * replace the received object properties into a received template HTML
  * @param html: temaplte HTML
  * @param obj: object to replace its propperties into the template
  * @return replaced HTML
  */
  replaceTemplateProperties(html, obj) {

    for (let prop in obj) {
      html = html.replace(new RegExp(`{{${prop}}}`, 'g'), obj[prop]);
    }

    return html;
  }

  /**
  * load()
  * call rest services
  * @param url: url to be called
  * @param callback: function to be called after service returns
  */
  load(url, callback) {

    const now = new Date().getTime();

    //prevent indiscriminately calls on low level
    if (now - this.lastCallTime < _commons_constants_js__WEBPACK_IMPORTED_MODULE_0__["default"].SEARCH_DELAY) {
      return;
    }

    this.lastCallTime = now;

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        callback(JSON.parse(this.responseText));
      }
    };

    xhttp.open('GET', url, false);
    xhttp.setRequestHeader('Authorization', `Bearer ${_commons_constants_js__WEBPACK_IMPORTED_MODULE_0__["default"].TOKEN}`);
    xhttp.send();
  }
}

/* harmony default export */ __webpack_exports__["default"] = (new Utils());

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_application_controller_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./services/application.controller.js */ "./src/services/application.controller.js");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.scss */ "./src/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_1__);



window.application = new _services_application_controller_js__WEBPACK_IMPORTED_MODULE_0__["default"]();
window.application.initialize();

/***/ }),

/***/ "./src/index.scss":
/*!************************!*\
  !*** ./src/index.scss ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin
    if(false) { var cssReload; }
  

/***/ }),

/***/ "./src/services/application.controller.js":
/*!************************************************!*\
  !*** ./src/services/application.controller.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ApplicationController; });
/* harmony import */ var _user_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.js */ "./src/services/user.js");
/* harmony import */ var _user_controller_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.controller.js */ "./src/services/user.controller.js");
/* harmony import */ var _commons_constants_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../commons/constants.js */ "./src/commons/constants.js");




class ApplicationController {

  constructor() {

    this.$searchInput = document.querySelector('input.search');
    this.searchTimeout = undefined;
  }

  initialize() {

    this.attachEventListeners();
    this.loadUser('');
  }

  /**
  * loadUser()
  * call gitlab user rest service
  * @param username: search user for the username
  * @param reset: if true, creates a new users list otherwise append the result to the existing one
  */
  loadUser(username = '', reset) {

    //avoid calling service to get the same username
    if (this.lastSearch === username && !!reset) {
      return false;
    }

    this.lastSearch = username;

    !!reset && _user_controller_js__WEBPACK_IMPORTED_MODULE_1__["default"].clear();
    _user_controller_js__WEBPACK_IMPORTED_MODULE_1__["default"].getByUserName(username, users => _user_controller_js__WEBPACK_IMPORTED_MODULE_1__["default"].renderAll(users));
  }

  /**
  * attachEventListeners()
  * attach listener to the DOM objects
  */
  attachEventListeners() {

    this.$searchInput.addEventListener('keyup', this._search.bind(this));
    document.addEventListener('scroll', this._loadMore.bind(this));
  }

  /**
  * _search()
  * interface to call loadUser()
  * it makes sure the service won't be loaded indiscriminately, respecting and interval
  * defined on Constants.SEARCH_DELAY
  * @see loadUser()
  */
  _search() {

    clearTimeout(this.searchTimeout);
    const call = this.loadUser.bind(this, this.$searchInput.value, true);
    this.searchTimeout = setTimeout(call, _commons_constants_js__WEBPACK_IMPORTED_MODULE_2__["default"].SEARCH_DELAY);
  }

  /**
  * _loadMore()
  * used to watch the window scroll position in order to load the next page
  * of users in case the user scrolls down to the end
  */
  _loadMore() {

    if (window.scrollY + window.innerHeight >= document.body.offsetHeight) {
      this.loadUser(this.$searchInput.value);
    }
  }
}

/***/ }),

/***/ "./src/services/user.controller.js":
/*!*****************************************!*\
  !*** ./src/services/user.controller.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _commons_constants_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../commons/constants.js */ "./src/commons/constants.js");
/* harmony import */ var _commons_utils_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../commons/utils.js */ "./src/commons/utils.js");
/* harmony import */ var _user_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.js */ "./src/services/user.js");




class UserController {

  constructor() {

    this.$userList = document.querySelector('.user-list');
    this.currentPage = -1;
  }

  /**
  * getByUserName()
  * call gitlab user rest service
  * @param username: search user for the username
  * @param callback: function to be called after service returns
  */
  getByUserName(username = '', callback) {

    _commons_utils_js__WEBPACK_IMPORTED_MODULE_1__["default"].load(`${_commons_constants_js__WEBPACK_IMPORTED_MODULE_0__["default"].REST_URL}/users?page=${++this.currentPage}&per_page=${_commons_constants_js__WEBPACK_IMPORTED_MODULE_0__["default"].PAGE_SIZE}&search=${username}`, result => {
      callback(result.map(c => new _user_js__WEBPACK_IMPORTED_MODULE_2__["default"](c)));
      //scroll to top in case it is a new search (not a new page)
      !!!this.currentPage && window.scrollTo(0, Math.min(90, window.scrollY));
    });
  }

  /**
  * clear()
  * emptys the users list DOM element and reset the current page
  */
  clear() {

    this.$userList.innerHTML = '';
    this.currentPage = -1;
  }

  /**
  * renderAll()
  * append list of users to the DOM element
  * @param users: list of users
  * @see user.js
  */
  renderAll(users) {

    //console.log(users);
    users.forEach(user => this.$userList.appendChild(user.render()));
  }
}

/* harmony default export */ __webpack_exports__["default"] = (new UserController());

/***/ }),

/***/ "./src/services/user.js":
/*!******************************!*\
  !*** ./src/services/user.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return User; });
/* harmony import */ var _commons_utils_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../commons/utils.js */ "./src/commons/utils.js");


class User {

  static get TEMPLATE_SELECTOR() {
    return '#template-user';
  }

  constructor({ id, name, username, state, avatar_url, web_url } = {}) {

    this.id = id;
    this.name = name;
    this.username = username;
    this.state = state;
    this.avatar_url = avatar_url;
    this.web_url = web_url;
  }

  /**
  * getTemplate()
  * @return user HTML template
  */
  getTemplate() {

    return document.querySelector(User.TEMPLATE_SELECTOR);
  }

  /**
  * render()
  * append the template's HTML into a runtime created DOM element
  * @return DOM element
  */
  render() {

    const $el = document.createElement('div');
    $el.innerHTML = _commons_utils_js__WEBPACK_IMPORTED_MODULE_0__["default"].replaceTemplateProperties(this.getTemplate().innerHTML, this);
    return $el;
  }
}

/***/ })

/******/ });
//# sourceMappingURL=main.js.map