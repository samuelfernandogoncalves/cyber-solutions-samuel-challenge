import Constants from '../commons/constants.js';

class Utils {

  constructor() {

    this.lastCallTime = 0;
  }

  /**
  * replaceTemplateProperties()
  * replace the received object properties into a received template HTML
  * @param html: temaplte HTML
  * @param obj: object to replace its propperties into the template
  * @return replaced HTML
  */
  replaceTemplateProperties(html, obj) {

    for(let prop in obj) {
      html = html.replace(new RegExp(`{{${prop}}}`, 'g'), obj[prop]);
    }

    return html;
  }

  /**
  * load()
  * call rest services
  * @param url: url to be called
  * @param callback: function to be called after service returns
  */
  load(url, callback) {

    const now = new Date().getTime();

    //prevent indiscriminately calls on low level
    if(now - this.lastCallTime < Constants.SEARCH_DELAY) {
      return;
    }

    this.lastCallTime = now;

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        callback(JSON.parse(this.responseText));
      }
    };

    xhttp.open('GET', url, false);
    xhttp.setRequestHeader('Authorization', `Bearer ${Constants.TOKEN}`);
    xhttp.send();
  }
}

export default new Utils();
