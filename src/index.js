import ApplicationController from './services/application.controller.js';
import CSS from './index.scss';

window.application = new ApplicationController();
window.application.initialize();
