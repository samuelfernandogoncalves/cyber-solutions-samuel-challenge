import User from './user.js';
import UserController from './user.controller.js';
import Constants from '../commons/constants.js';

export default class ApplicationController {


  constructor() {

    this.$searchInput = document.querySelector('input.search');
    this.searchTimeout = undefined;
  }

  initialize() {

    this.attachEventListeners();
    this.loadUser('');
  }

  /**
  * loadUser()
  * call gitlab user rest service
  * @param username: search user for the username
  * @param reset: if true, creates a new users list otherwise append the result to the existing one
  */
  loadUser(username = '', reset) {

    //avoid calling service to get the same username
    if(this.lastSearch === username && !!reset) {
      return false;
    }

    this.lastSearch = username;

    !!reset && UserController.clear();
    UserController.getByUserName(username, users => UserController.renderAll(users));
  }

  /**
  * attachEventListeners()
  * attach listener to the DOM objects
  */
  attachEventListeners() {

    this.$searchInput.addEventListener('keyup', this._search.bind(this));
    document.addEventListener('scroll', this._loadMore.bind(this));
  }

  /**
  * _search()
  * interface to call loadUser()
  * it makes sure the service won't be loaded indiscriminately, respecting and interval
  * defined on Constants.SEARCH_DELAY
  * @see loadUser()
  */
  _search() {

    clearTimeout(this.searchTimeout);
    const call = this.loadUser.bind(this, this.$searchInput.value, true);
    this.searchTimeout = setTimeout(call, Constants.SEARCH_DELAY);

  }

  /**
  * _loadMore()
  * used to watch the window scroll position in order to load the next page
  * of users in case the user scrolls down to the end
  */
  _loadMore() {

    if((window.scrollY + window.innerHeight) >= document.body.offsetHeight) {
      this.loadUser(this.$searchInput.value);
    }
  }
}
