import Utils from '../commons/utils.js';

export default class User {

  static get TEMPLATE_SELECTOR() { return '#template-user' };

  constructor({id, name, username, state, avatar_url, web_url} = {}) {

    this.id = id;
    this.name = name;
    this.username = username;
    this.state = state;
    this.avatar_url = avatar_url;
    this.web_url = web_url;
  }

  /**
  * getTemplate()
  * @return user HTML template
  */
  getTemplate() {

    return document.querySelector(User.TEMPLATE_SELECTOR);
  }

  /**
  * render()
  * append the template's HTML into a runtime created DOM element
  * @return DOM element
  */
  render() {

    const $el = document.createElement('div');
    $el.innerHTML = Utils.replaceTemplateProperties(this.getTemplate().innerHTML, this);
    return $el;
  }
}


