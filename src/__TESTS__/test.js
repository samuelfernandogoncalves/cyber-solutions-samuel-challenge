import UserController from '../services/user.controller.js';
import Constants from '../commons/constants.js';
import ApplicationController from '../services/application.controller.js';

const AppCtrl = new ApplicationController();

describe('Rest service', function () {
  it('shoud load users', function () {    

    UserController.getByUserName('', users => {
      chai.expect(users.length).to.equal(20);
    });

  });

  it('shoud not call the service twice with the same username', function () {    
    try {
      AppCtrl.loadUser('sam', true);
    } catch(e) {}
    const call = AppCtrl.loadUser('sam', true) === false;
    chai.expect(call).to.equal(true);    
  });

});

describe('Page control', function () {
  it('shoud respect page size', function () {    

    Constants.PAGE_SIZE = 10;

    UserController.getByUserName('', users => {
      chai.expect(users.length).to.equal(10);
    });

  });
});