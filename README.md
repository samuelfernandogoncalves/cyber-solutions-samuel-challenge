# Cyber Solutions Challenge #
 
  
This project uses ECMAScript modules and it was developed using only vanilla Javascript.
Webpack is used to build it into "/dist" folder.

To run testings a webserver is nedded. Although it is automatically provided by building it.


> Note  
> Sources are in "/src" folder.

Installing

```bash
# Install dependencies
npm install
```

Building

```bash
# Generate a new build and start the webserver
npm run serve

# It will be available on:
# http://localhost:8080
```

> Note  
> Tests are available on http://localhost:8080/tests.html

